
'''from ..dashboards.reporter import Reporter
from ..dashboards.visualizer import Visualizer
from ..dashboards.signalizer import Signalizer
from ..dashboards.conclusionizer import Conclusion'''
import numpy as np
from utils.cont_variable import visualize_distribution, stat_analysis
from utils.category_variable import category_distribution

class EDA():

    def __init__(self, test_name='EDA'):
        '''
            Как содержит функции полного цикла
            части анализа данных. В функции включены
            части проставления сигнала, словесного объяснения

            все функции работают на основе Pandas. И дополнительных
            уникальных аргументов. Предусматривает вложенности как 
            в Reporter.
        '''
        self.test_name = test_name
        return
    
    def check_stats_analysis(self, df, variables:list, group:str=None):
        '''
            Функция проводит полный анализ распределения
            количественной переменной. 

            1) Тесты на нормальность
            2) Тесты на скосы и эксцессы
            3) Визуализация распределения
            4) Визуализация выбросов
        '''
        numeric = df.select_dtypes(include=[np.number])
        category = df.select_dtypes(exclude=[np.number])
        report = {}
        report['histogram'] = {}
        report['stats'] = {}
        for variable in variables:
            if variable in numeric:
                report['histogram'][variable] = visualize_distribution(df, variable=variable, group=group)
                report['stats'][variable] = stat_analysis(df, variable=variable)
            elif variable in category:
                report['histogram'][variable] = category_distribution(df, variable=variable, group=group)
        return report
    
    def check_target(self, df, target_name, target_type, date_name=None, date_group='month', submodel='overall', title='Анализ целевой переменной', sub_title=''):
        '''
            Функция проводит полный анализ целевой переменной.
            Позволяет получить анализ и рекомендации по целевой переменной, которых будет
            достаточно для более верного анализа и правильного выбора модели

            Включает в себя:
            1) Выведение основных статистик по целевой переменной
            2) Анализ распределения целевой переменной

            # Если переданно время
            3) Анализ тренда целевой переменной
            4) Анализ стабильности целевой переменной во времени

        '''
        if date_name != None:
            if target_type == 'categorial':
                number_miss = df['target']
                self.report.setdefault(submodel, {})
            unuqie_values = target_name
        return