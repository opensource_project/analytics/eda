import plotly.express as px
import plotly.graph_objects as go
import pandas as pd

import scipy.stats as stats

def visualize_distribution(df:pd.DataFrame, variable:list, group:str=None):
    df = df.copy().dropna()
    fig = go.Figure()
    if group == None:
        fig.add_trace(go.Histogram(x=df[variable]))
    else:
        unique = df[group].unique()
        for val in unique:
            df_val = df[df[group] == val]
            fig.add_trace(go.Histogram(x=df_val[variable]))
    fig.update_traces(opacity=0.75)
    fig.update_layout(barmode='overlay')
    return fig


def stat_analysis(df:pd.DataFrame, variable:list):
    '''
        Рассчитывает следующие тесты для колличественной переменной

        1) Шапиро-вилка

        * автматически удаляет миссинги из столбцов
    '''
    report = {}
    # Нормальность
    _, p_value = stats.shapiro(df[variable].dropna())
    is_norm = p_value > 0.05
    if is_norm:
        report['Нормальность'] = f'Переменная распределена нормально. p value - {p_value}'
    else:
        report['Нормальность'] = f'Переменная не распределена нормально. p_value - {p_value}'
    return report
