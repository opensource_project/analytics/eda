

import pandas as pd
import numpy as np 
import plotly.graph_objects as go 


def category_distribution(df:pd.DataFrame, variable:str, group:str=None):

    fig = go.Figure()
    if group != None:
        fig.add_trace(go.Histogram(x=df[variable], name="count"))
    return fig

